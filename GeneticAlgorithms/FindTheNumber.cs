﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithms
{
    public class FindTheNumber
    {
        Random random;
        List<int> currentPopulation;
        List<int> newPopulation;
        int target;
        int crossingOverRate;
        int populationSize;
        int generationCounter = 0;
        int generations;
        
        float topPercent;
        float mutationThreshold;
        int maxMutationsPerChild;

        // Constructor ------------------------------------------------------------------
        // public -----------------------------------------------------------------------

        public FindTheNumber(int target, int crossingOverRate, float mutationThreshold, int maxMutationsPerChild, int populationSize, float topPercent, int generations)
        {
            this.target = target;
            this.crossingOverRate = crossingOverRate > 32 ? 32 : crossingOverRate;
            this.crossingOverRate = crossingOverRate < 0 ? 0 : crossingOverRate;
            this.populationSize = populationSize;
            this.mutationThreshold = mutationThreshold;
            this.maxMutationsPerChild = maxMutationsPerChild;
            this.topPercent = topPercent;
            this.generations = generations;

            random = new Random();
        }

        public void StartCalculation()
        {
            PopulateFirstGeneration();
            for (int i = 0; i < generations; i++)
            {
                LetPopulationBreed();
            }
        }

        // Populate the first generation ------------------------------------------------
        // private ----------------------------------------------------------------------

        private void PopulateFirstGeneration()
        {
            generationCounter++;
            currentPopulation = new List<int>();

            for (int i = 0; i < populationSize; i++)
            {
                int value = GetRandomInt();
                currentPopulation.Add(value);
            }

            List<int> sortedPopulation = currentPopulation.OrderBy(x => Compare(x)).ToList();
            currentPopulation = sortedPopulation;

            Console.WriteLine("Top 10 in generation 1 :");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(currentPopulation[i]);
            }
        }

        // Let Population breed ---------------------------------------------------------
        // private ----------------------------------------------------------------------

        private void LetPopulationBreed()
        {
            generationCounter++;
            Console.WriteLine("Breeding generation " + generationCounter + "...");
            newPopulation = new List<int>();
            int maximumIndex = (int)(populationSize * topPercent);

            for (int i = 0; i < populationSize; i ++)
            {
                int mom = currentPopulation[random.Next(maximumIndex)];
                int dad = currentPopulation[random.Next(maximumIndex)];

                newPopulation.Add(CreateNewChildFromParents(mom, dad));
            }
                        
            currentPopulation = newPopulation.OrderBy(x => Compare(x)).ToList();

            Console.WriteLine("Top 10 in generation " + generationCounter + ":");
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(currentPopulation[i]);
            }

        }

        public int Compare(int x)
        {
            try
            {
                if (x > target)
                {
                    return Math.Abs(x - target);
                }
                else
                {
                    return Math.Abs(target - x);
                }
            }
            catch (ArithmeticException)
            {
                return int.MaxValue;
            }
        }

        // Create new child from parents ------------------------------------------------
        // private ----------------------------------------------------------------------

        private int CreateNewChildFromParents(int mom, int dad)
        {
            int index = 0;
            BitArray momArray = GetBitarrayFromInt(mom);
            BitArray dadArray = GetBitarrayFromInt(dad);

            for (int i = 0; i < crossingOverRate; i++)
            {
                index = random.Next(32);
                dadArray[index] = momArray[index];
            }

            for (int i = 0; i < maxMutationsPerChild; i++)
            {
                float mutation = (float)random.NextDouble();
                if (mutation > mutationThreshold)
                {
                    index = random.Next(32);
                    dadArray[index] = dadArray[index] == true ? false : true;
                }
            }

            return GetIntFromBitArray(dadArray);
        }


        // Get Bitarray from int --------------------------------------------------------
        // private ----------------------------------------------------------------------

        private BitArray GetBitarrayFromInt(int value)
        {
            return new BitArray(new int[] { value });
        }

        // Get Int From Bit array -------------------------------------------------------
        // private ----------------------------------------------------------------------

        private int GetIntFromBitArray(BitArray bitArray)
        {
            if (bitArray.Length > 32)
                throw new ArgumentException("Argument length shall be at most 32 bits.");

            int[] array = new int[1];
            bitArray.CopyTo(array, 0);
            return array[0];
        }

        // Get Random int ---------------------------------------------------------------
        // private ----------------------------------------------------------------------

        private int GetRandomInt()
        {
            int value = random.Next();
            if (random.NextDouble() > 0.5f)
                value = -value;
            return value;
        }
    }
}
