﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithms
{
    class Program
    {
        static void Main(string[] args)
        {
            FindTheNumber ftn = new FindTheNumber(124, 16, 0.8f, 1, 100000, 0.2f, 30);
            ftn.StartCalculation();
            Console.ReadKey();
        }
    }
}
