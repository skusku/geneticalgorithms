﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticAlgorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithms.Tests
{
    [TestClass()]
    public class FindTheNumberTests
    {
        [TestMethod()]
        public void CompareTest()
        {
            FindTheNumber ftn = new FindTheNumber(-100, 16, 0.8f, 1, 100, 0.2f, 15);
            Assert.AreEqual(ftn.Compare(2147483647), 10);

        }
    }
}